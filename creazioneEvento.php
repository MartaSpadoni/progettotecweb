<?php
require_once 'connection.php';

$templateParams["titolo"] = "JumpTheLine - CreazioneEvento";
$templateParams["pagina"] = "creazioneEvento.php";

if(isset($_SESSION["idUtente"])){
   $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }
$templateParams["categorie"] = $dbh->getCategories();

if(isset($_POST["nome"]) && isset($_POST["descr_breve"]) && isset($_POST["descr_lunga"]) && isset($_POST["categoria"]) && isset($_FILES["immagine_inserita"]))
{  
    list($result, $msg) = uploadImage("./upload/", $_FILES["immagine_inserita"]);
    
    if($result != 0){
        $imgarticolo = $msg;
    }

    $nome = $_POST["nome"];
    $desc_breve = $_POST["descr_breve"];
    $desc_lunga = $_POST["descr_lunga"];
    $categoria = $_POST["categoria"];
    $organizzatore = $dbh->getUserByEmail($_SESSION['email'])["Codice"];
    $templateParams["evento"] = $dbh->insertEvent($nome, $desc_breve, $desc_lunga, $categoria, $organizzatore, $imgarticolo);

    for($i=1; array_key_exists('data'.$i, $_POST); $i++) {
      $data = $_POST["data".$i];
      $ora = $_POST["ora".$i];
      $luogo = $_POST["luogo".$i];
      $biglietti = $_POST["biglietti".$i];   
      $prezzo = $_POST["prezzo".$i]; 
      $templateParams["dettaglio"] = $dbh->insertDetail($data, $ora, $luogo, $biglietti, $nome, $prezzo);                                                                    
    }

    if($templateParams["evento"] && $templateParams["dettaglio"]) {
      $templateParams["success"] = "L'evento è stato creato correttamente";
    } else {
      $templateParams["failure"] = "La creazione dell'evento non è andata a buon fine";
    }
}

require 'template/base.php';
?>