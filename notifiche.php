<?php
require_once 'connection.php';  
$templateParams["titolo"] = "JumpTheLine - Notifiche";
$templateParams["pagina"] = "notifiche.php";
if(isset($_GET["id"])){
   $dbh->setNotificationRead($_GET["id"]);
}
if(isset($_SESSION["idUtente"])){
    $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }

 if(isset($_GET["elimina"]) && $_GET["elimina"]){
    $dbh->deleteAllUsersNotifications($_SESSION["idUtente"]);
 }
 if(isset($_GET["eliminaNot"])){
   $dbh->deleteUsersNotifications($_SESSION["idUtente"], $_GET["eliminaNot"]);
}

 $templateParams["notificheLette"] = $dbh->getReadNotifications($_SESSION["idUtente"]);
 $templateParams["notificheDaLeggere"] = $dbh->getUnreadNotifications($_SESSION["idUtente"]);
require 'template/base.php';
?>