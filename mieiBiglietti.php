<?php
require_once 'connection.php';
//notifiche
if(isset($_SESSION["idUtente"])){
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }
 //base Template
 $templateParams["titolo"] = "JumpTheLine - I miei biglietti";
 $templateParams["pagina"] = "mieiBiglietti.php";

 $templateParams["ordini"]=$dbh->myOrder($_SESSION["idUtente"]);

 require 'template/base.php';
 ?>