<?php
require_once 'connection.php';
//notifiche
if(isset($_SESSION["idUtente"])){
   $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }
 //base Template
 $templateParams["titolo"] = "JumpTheLine - Gestisci Organizzatore";
 $templateParams["pagina"] = "gestisciOrganizzatore.php";

 $templateParams["organizzatori"]=$dbh->getOrganizer();
 if(isset($_GET["organizzatore"])){
    $dbh->removeOrganizer($_GET["organizzatore"]);
    $templateParams["organizzatori"]=$dbh->getOrganizer();
   }
   
 require 'template/base.php';
?>