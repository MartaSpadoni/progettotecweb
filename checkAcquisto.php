<?php
require_once 'connection.php';
$b = $dbh->getAvailableTickets($_POST["id"]);
if(intval($b["BigliettiDisponibili"]) >= intval($_POST["numeroBiglietti"])){
    $b["BigliettiDisponibili"] = intval($b["BigliettiDisponibili"]) - intval($_POST["numeroBiglietti"]);
    $dbh->updateAvailableTickets($_POST["id"], $b["BigliettiDisponibili"]);
    $dbh->insertOrder(intval($_POST["id"]), $_POST["numeroBiglietti"], $_SESSION["idUtente"]);
    $result["res"]=true;
    $result["code"]= '<div class="panel panel-success">
    <div class="panel-heading">Acquisto completato</div>
    <div class="panel-body">Le confermiamo che il suo acquisto è avvenuto con successo!</div>
    </div>
    <a href="index.php" class="btn btn-primary" role="button">Torna alla Home</a>';
}else{
  $result["res"]=false;
  $result["code"]='<div class="panel panel-danger">
    <div class="panel-heading">Errore completamento acquisto</div>
    <div class="panel-body"> Ci scusiamo per il disguido ma non possiamo procedere con il suo acquisto</div>
  </div>
  <a href="index.php" class="btn btn-primary" role="button">Torna alla Home</a>'; 
}

print json_encode($result);
?>