<?php
require_once 'connection.php';

$templateParams["titolo"] = "JumpTheLine - Registration";
$templateParams["pagina"] = "registration.php";
if(isset($_SESSION["idUtente"])){
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
   $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
   if($numeroNotifiche > 0){
      $templateParams["numeroNotifiche"] = $numeroNotifiche;
   }else{
      $templateParams["numeroNotifiche"]="";
   }
}else{
   $templateParams["numeroNotifiche"] = "";
}

if(isset($_POST["email"]) && isset($_POST["p"])){
      // Recupero la password criptata dal form di inserimento.
      $password = $_POST['p'];
      $email = $_POST['email'];
      
      if(!empty($dbh->getUserByEmail($email))) {
         $templateParams["failure"] = 'La registrazione non è andata a buon fine. Esiste già un account con questa email';
      } else {
         if(strlen($_POST["p"])>=8) {
            // Crea una chiave casuale
            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
            // Crea una password usando la chiave appena creata.
            $password = hash('sha512', $password.$random_salt);
            //inserisco utente nel db
            $nome = $_POST['nome'];
            $cognome = $_POST['cognome'];
            $tipo = isset($_POST['organizzatore']) ? 'organizzatore' : 'cliente';
            $data = $_POST['data'];
            $indirizzo = $_POST['indirizzo'];
            $result = $dbh->insertUser($nome, $cognome, $email, $tipo, $password, $random_salt, $data, $indirizzo);
            if($result) {
               $templateParams["success"] = "La registrazione è stata effettuata con successo!";
            } else {
               $templateParams["failure"] = "La registrazione non è andata a buon fine.";
            }
         } else {
            $templateParams["failure"] = "Registrazione fallita. La password deve essere di almeno 8 caratteri";
         }
      }
}

require 'template/base.php';
?>