<div class="container-fluid" id="container-dati">
    <h3>I miei dati</h3>
    <div class="cerchio" id="circle">
        <i class="material-icons" id="icona">person</i>
    </div>
    <div id="dati">
        <label for="nome">Nome</label> <?php echo $templateParams["datiUtente"]["Nome"]?>
        </i>
        </br>
        <label for="cognome">Cognome</label> <?php echo $templateParams["datiUtente"]["Cognome"]?>
        </br>
        <label for="data">Data nascita</label> <?php echo $templateParams["datiUtente"]["DataNascita"]?>
        </br>
        <label for="indirizzo">Indirizzo</label> <?php echo $templateParams["datiUtente"]["Indirizzo"]?>
        </br>
        </br>
        <button class="btn btn-primary" id="modifica" type="button" name="button" data-toggle="modal" data-target="#myModalDati">Modifica</button>
    </div>
    <hr>
    <div class="cerchio" id="circle">
        <i class="material-icons" id="icona">lock</i>
    </div>
    <div id="pass">
        <label for="indirizzo">Password</label>
        </br>
        <button class="btn btn-primary" id="modifica" type="button" name="button" data-toggle="modal" data-target="#myModalPass">Modifica password</button>
        <?php if(isset($_GET["messaggio"])) :?>
          </br>
          <p> <?php echo $_GET["messaggio"]?> </p>
        <?php endif;?>
      </div>
</div>

  <!-- Modal dati -->
  <form action="datiUtente.php" method="post">
  <div class="modal" id="myModalDati" role="dialog"> <!-- dialog per accessibilità -->
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modifica i tuoi dati</h4>
          <p>Lascia vuoti i campi che non vuoi cambiare</p>
        </div>
        <div class="modal-body">
          <label for="nome">Nome</label></br>
          <input type="text" id="nome" name="nome"></br>
          <label for="cognome">Cognome</label></br>
          <input type="text" id="cognome" name="cognome"></br>
          <label for="data">Data di nascita</label></br>
          <input type="date" id="data" name="data"></br>
          <label for="indirizzo">Indirizzo</label></br>
          <input type="text" id="ind" name="ind">
        </div>
        <div class="modal-footer">
          <button type="submit" value="modifica" id="modModifica">Modifica</button>
        </div>
      </div>
    </div>
    </div>

    <!-- Modal pass -->
  <div class="modal" id="myModalPass" role="dialog"> <!-- dialog per accessibilità -->
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modifica la tua password</h4>
        </div>
        <div class="modal-body">
            <label for="password">Nuova password</label></br>
            <input type="password" id="password" name="password"></br>
        </div>
        <div class="modal-footer">
          <button type="submit" value="modifica">Modifica</button>
        </div>
      </div>
    </div>
</form>