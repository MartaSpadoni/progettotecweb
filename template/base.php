<!DOCTYPE html>
<html lang="it">
<head>
  <title><?php echo $templateParams["titolo"];?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="template.css" rel="stylesheet" type="text/css"/>
  <link href="card.css" rel="stylesheet" type="text/css"/>
</head>
<body>
    <nav class="navbar navbar-inverse" id="mainNav">
        <div class="container-fluid">
            <div class="navbar-header" id="NavbarMenu">
            <?php if(!isUserLoggedIn() || $_SESSION["tipo"] == "cliente"):?>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" >
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
            <?php endif; ?>
                <a class="navbar-brand" id="title" href="index.php">JumpTheLine</a>
                <div class="navbar-header navbar-right" id="navbarIconMenu">
                    <a class="navbar-brand navbar-right" <?php if(isUserLoggedIn()):?> href=areaUtente.php <?php else: ?> href=login.php <?php endif; ?> id="userIcon"><span class="glyphicon glyphicon-user"></span><span id="nomeNav"><?php if(isUserLoggedIn()){ echo " ".$templateParams["user"]["Nome"]; }?></span></a> 
                    <a class="navbar-brand navbar-right" <?php if(isUserLoggedIn()):?> href="notifiche.php" <?php else: ?>  href=login.php <?php endif; ?>  id="inbox"><span class="glyphicon glyphicon-inbox"></span> <span><?php echo $templateParams["numeroNotifiche"] ?></span></a>
                </div>
            </div>
        </div>
    </nav>
    <?php if(!isUserLoggedIn() || $_SESSION["tipo"] == "cliente"):?>
    <nav class="navbar navbar-inverse" id="secondarynav">
        <div>
            <ul class="collapse  navbar-collapse nav navbar-nav" id="myNavbar">
                <li><a href="categoria.php?idcategoria=1&nome=Concerti">Concerti</a></li>
                <li><a href="categoria.php?idcategoria=2&nome=Sport">Sport</a></li>
                <li><a href="categoria.php?idcategoria=3&nome=Musei">Musei</a></li>
                <li><a href="categoria.php?idcategoria=4&nome=Teatro">Teatro</a></li>
            </ul>
            <form class="form-inline " action="eventoCercato.php" method="GET" id="navSearch">
                <input class="form-control mr-sm-2" type="search" placeholder="Cerca" aria-label="Search by event" id="searchBar" name="search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit" id="searchButton">Cerca</button>
            </form>
        </div>
    </nav>
    <?php endif; ?>

<main>
    <?php
        if(isset($templateParams["pagina"])){
            require($templateParams["pagina"]);
        }
    ?>
    <br><br><br>
</main>

<footer class="text-center container-fluid" id="footer">
  <a href="help.php">Help & Contatti</a>
</footer>
<script type="text/javascript" src="sha512.js"></script>
<script type="text/javascript" src="forms.js"></script>

</body>
</html>