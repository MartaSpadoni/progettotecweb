<div class="container">
  <h3 id="titoloGestisciOrganizzatore">Organizzatori</h3>
      <?php foreach( $templateParams["organizzatori"] as $organizzatore):?>
        <div class="row">
            <div class="card text-center" id="OrganizzatorCard">
                    <div class="card-header">
                        <h5 class="card-title" id="titoloGestisciOrganizzatoreStampato"><?php echo $organizzatore["Nome"]." ".$organizzatore["Cognome"]?></h5>
                    </div>
                <div class="card-body">
                <p class="card-text text-left testoGestisciEventi"><span>Data di nascita: </span><?php echo $organizzatore["DataNascita"]?></p>
                    <p class="card-text text-left testoGestisciEventi"><span>Residenza: </span><?php echo $organizzatore["Indirizzo"]?></p>
                    <p class="card-text text-left testoGestisciEventi"><span>E-Mail: </span><?php echo $organizzatore["Email"] ?></p>

                  <button class="btn btn-primary" onclick=sendNotification(<?php echo $organizzatore["Codice"]?>)>Elimina Organizzatore</button>                  
                </div>
            </div>
        </div>
      <?php endforeach;?>
</div>
<script>
  function sendNotification(cod) {
    $.post("sendNotification.php", {type:5, codOrganizzatore:cod}, function(data) {
           window.location.assign("gestisciOrganizzatore.php?organizzatore="+cod);
        });
  }
</script>