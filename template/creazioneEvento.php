<div class="container"></div>
<form class="formEvento" enctype="multipart/form-data" action='creazioneEvento.php' method="POST">
    <!-- contenitore responsivo fluido -->
    <div class="container" id="container-evento">
        <div class="row">
            <h3>Crea un nuovo evento!</h3></br>

            <?php if(isset($templateParams["success"])):?>
                <div class="alert alert-success" id="successo">
                    <p><?php echo $templateParams["success"] ?></p> 
                </div>
            <?php endif; ?>
    
            <?php if(isset($templateParams["failure"])): ?>
                <div class="alert alert-danger" id="insuccesso">
                    <p><?php echo $templateParams["failure"] ?></p> 
                </div>
            <?php endif; ?>

            <div class="control-group">
            <!-- Nome e Descrizioni-->
            <label class="control-label"  for="nome">Nome</label>
            <div class="controls">
                <input type="text" id="nome" name="nome" placeholder="" class="input-xlarge" required>
            </div>
            </br>
            <label class="control-label"  for="descr_breve">Descrizione breve</label>
            <div class="controls">
                <textarea name="descr_breve" id="descr_breve" cols="30" rows="5" required></textarea>
            </div>
            </br>
            <label class="control-label"  for="descr_lunga">Descrizione lunga</label>
            <div class="controls">
                <textarea name="descr_lunga" id="descr_lunga" cols="35" rows="5" required></textarea>
            </div>
            </br>
        </div>
        <div class="row">
            <div class="col-sm-2" id="cat">
                <label for="categoria" id="categoria">Categoria</label></br>
                <?php foreach($templateParams["categorie"] as $categoria): ?>
                    <input type="radio" name="categoria" value=<?php echo $categoria["Nome"]; ?> required><?php echo $categoria["Nome"]; ?><br>
                <?php endforeach; ?>
            </div>
            </br>
            <div class="col-sm-4" id="immagine">
                <label for="immagine" id="ins_immagine">Inserisci Immagine</label>
                <input type="file" id="immagine_inserita" name="immagine_inserita" required> 
            </div>
        </div>  
        <hr>
        <h3>Inserisci il dettaglio</h3>
        <div class="table-responsive" id="dettaglio">
            <table class="table table-hover" id="table">
                <thead>
                <tr>
                    <th>Data</th>
                    <th>Ora</th>
                    <th>Luogo</th>
                    <th>Biglietti totali</th>
                    <th>Prezzo</th>
                </tr>
                </thead>
                <tbody class="text-center"> </tbody>
            </table> 

            <input type="button" id="aggiungiDettaglio" onclick="addRow()" value="Aggiungi">
        </div>
        </br>
        <div class="row">
            <button class="btn-success" id="crea">Crea</button>
        </div>
    </div>
</form>
</div>

<body onLoad="addRow()" >
</body>