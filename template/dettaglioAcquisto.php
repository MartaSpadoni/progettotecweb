<div class="container" id="Carrello">
    <h1>Carrello</h1>
    <div class="progress">
        <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="60" style="width:100%">
            Hai ancora un minuto per terminare il tuo acquisto
        </div>
    </div>
    <h3>Acquista i biglietti per <?php echo $templateParams["evento"]["nome"];?></h3>
    <div class="row">
        <div class="col-xs-6">
            <img class="img-responsive" src="upload/<?php echo $templateParams["evento"]["immagine"];?>" alt="" style="width:100%">
        </div>
        <div class="col-xs-6 text-left" id="datiDettaglioEvento">
            <p> Data: <?php echo date("d/m/Y", strtotime($templateParams["dettagliEvento"]["Data"]));?></p></br> 
            <p> Ora: <?php echo substr($templateParams["dettagliEvento"]["Ora"], 0, 5);?></p></br>
            <p class="Luogo"> Luogo: </p><p class="Luogo"><?php echo " ".$templateParams["dettagliEvento"]["Luogo"]?></p></br><br>
            <p> Prezzo unitario: € <?php echo $templateParams["dettagliEvento"]["Prezzo"]?></p></br>
        </div>
    </div>
        <div id="Descr"><p><?php echo $templateParams["evento"]["DescrLunga"];?></p></div>
        <div id="selBiglietti">
            <div class="container-fluid text-center">
                <h5>Seleziona il numero di biglietti</h5>
                <button class="btn btn-primary" id="meno" onclick=meno(<?php echo $templateParams["dettagliEvento"]["Prezzo"]?>)>-</button><span id="numeroBiglietti">1</span><button class="btn btn-primary" id = "aggiungi" onclick=aggiungi(<?php echo $templateParams["dettagliEvento"]["Prezzo"]?>)>+</button>
            </div>
            <div class="containert-fluid text-center"> <h4 id="Totale">Totale: €</h4> <p id="tot"><?php echo $templateParams["dettagliEvento"]["Prezzo"]?></p></div>
            <div class="container-fluid text-center"><button class="btn btn-primary" onclick=sendNotification(<?php echo $templateParams["dettagliEvento"]["Codice"]?>)>Acquista</button></div> 
        </div>
</div>

<script>
    var timer;
    $("document").ready(function() {
        var s = 0;
        timer = setInterval(() => {
            s=s+1;
            let t = 60-s;
            $(".progress-bar").attr("aria-valuenow", t);
            $(".progress-bar").attr("style", "width:"+(t*10/6)+"%");
            $(".progress-bar").text(t + " secondi per terminare il tuo acquisto");
            if(t <= 0){
                window.history.back();
            }
        }, 1000);
    });
function sendNotification(codEvento) {
   $.post("checkAcquisto.php", {id:codEvento, numeroBiglietti:$("#numeroBiglietti").text()}, function(data){
       data=JSON.parse(data);
       if(data.res){
           $.post("sendNotification.php", {type:1, numBiglietti:$("#numeroBiglietti").text(), codiceEvento:codEvento});
       }
           clearInterval(timer);
           $("#Carrello").html(data.code);
   })
}
function meno(p) {
    let n = $("#numeroBiglietti").text();
    if(parseInt(n)-1 >= 1){
        n = parseInt(n)-1;
        $("#numeroBiglietti").text(n);
        $("#tot").text(n*p);

    }

}
function aggiungi(p) {
    let n = $("#numeroBiglietti").text();
    if(parseInt(n)+1 <= 6){
        n = parseInt(n)+1;
        $("#numeroBiglietti").text(n);
        $("#tot").text(n*p);

    }


}
</script>
