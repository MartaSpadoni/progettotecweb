    <div class="container">
        <div class="row" id="RigaTitoloArea">
          <br>
          <h5 id="titoloArea">Area utente: <?php echo $templateParams["utente"]["Tipo"]?></h5><a href="logout.php" class="btn btn-primary" role="button"><span class="glyphicon glyphicon-log-out"></span></a>
          <p id="nomeUtente"><?php echo $templateParams["utente"]["Nome"]." ".$templateParams["utente"]["Cognome"] ?></p>
        </div>
      <?php $i = 0;?>
        <div class="row rowArea">
        <?php foreach($templateParams["sezioni"] as $s):?>
          <?php if( $i != 0 && $i % 2 == 0):?>
            </div>
            <div class="row">
          <?php endif;?>
            <?php $i++ ;?>
            <a href="<?php echo $s[2]?>" id="divLink"><div class="col-xs-6 text-center" id="divArea"><div id="interno"><img src="img/<?php echo $s[1]?>" alt="Dati Utente" class="img-fluid"><h3> <?php echo $s[0] ?></h3></div></div></a>
        <?php endforeach;?>
    </div>