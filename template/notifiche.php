<div class="container">
    <h3>Notifiche da leggere</h3>
    <?php if(empty($templateParams["notificheDaLeggere"])):?>
        <p>Non ci sono nuove notifiche.</p>
    <?php else: ?>
        <div class="card-deck">
            <?php foreach($templateParams["notificheDaLeggere"] as $notifica):?>
            <div class="card" id="notifica" >
                <div class="card-header">
                <h4 class="card-title">Messaggio da: <?php echo $notifica["nome"]." ".$notifica["cognome"] ?></h4>
                </div>
                <div class="card-body">
                <p class="card-text"><?php echo $notifica["titolo"]?></p>
                <p class="card-text collapse" id="fullText<?php echo $notifica["codice"]?>"> <?php echo $notifica["messaggio"]?></p>
                <p class="card-text">Ricevuta il <?php echo $notifica["data"]?></p>
                <button class="btn btn-primary" data-toggle="collapse" data-target="#fullText<?php echo $notifica["codice"]?>" id="<?php echo $notifica["codice"]?>" onclick=segnaComeLetta(<?php echo $notifica["codice"]?>)>Leggi</button>
                <button class="btn btn-danger eliminaNotifiche"   onclick=deleteNotification(<?php echo $notifica["codice"]?>) > <span class="glyphicon glyphicon-trash"></span> </button>    
            </div>
            </div>
            <?php endforeach;?>
        </div>
    <?php endif;?>
    <h3>Notifiche lette</h3>
    <?php if(empty($templateParams["notificheLette"])):?>
        <p>Non ci sono notifiche.</p>
    <?php else: ?>
        <div class="card-deck">
        <?php foreach($templateParams["notificheLette"] as $notifica):?>
        <div class="card" id="notifica" >
            <div class="card-header">
            <h4 class="card-title">Messaggio da: <?php echo $notifica["nome"]." ".$notifica["cognome"] ?></h4>
            </div>
            <div class="card-body">
            <p class="card-text"><?php echo $notifica["titolo"]?></p>
            <p class="card-text collapse" id="fullText<?php echo $notifica["codice"]?>"> <?php echo $notifica["messaggio"]?></p>
            <p class="card-text">Ricevuta il <?php echo $notifica["data"]?></p>
            <button class="btn btn-primary" data-toggle="collapse" data-target="#fullText<?php echo $notifica["codice"]?>" id="<?php echo $notifica["codice"]?>" onclick=changeText(<?php echo $notifica["codice"]?>)>Leggi</button>
            <button class="btn btn-danger eliminaNotifiche"   onclick=deleteNotification(<?php echo $notifica["codice"]?>)><span class="glyphicon glyphicon-trash"></span></button>
            </div>
        </div>
        <?php endforeach;?>
        </div>
        <?php endif;?>
</div>

<script>
    function changeText(e){
        if($("#"+e).text() == "Leggi"){
            $("#"+e).text("Chiudi");
        }else{
            $("#"+e).text("Leggi");


        }
    }
   function segnaComeLetta(e){
    if($("#"+e).text() == "Leggi"){
            $("#"+e).text("Chiudi");
        }else{
            $("#"+e).text("Leggi");
            $.get("notifiche.php",{id:e}, function(data) {
                window.location.reload();
            });

        }
    }

    function deleteNotification(cod) {
        $.get("notifiche.php",{eliminaNot:cod}, function(data) {
            window.location.reload();
        });
    }
</script>