<div class="container">
  <h1 id="titoloMieiBiglietti">I miei biglietti</h1>
      <?php foreach( $templateParams["ordini"] as $evento):?>
        <div class="row">
              <div class="card text-center"  id="eventoCard">
                  <img  src="upload/<?php echo $evento["NomeImmagine"] ?>" class="card-img-top immaginiBiglietti" alt="Image">
                    <div class="card-header">
                       <h5 class="card-title" id="titoloGestisciEventoStampato"><?php echo $evento["nome"]?></h5>
                    </div>
                  <div class="card-body biglietto">
                    <p class="card-text text-left testoGestisciEventi"><span>Data Acquisto: </span><?php echo$evento["dataOrdine"]?></p>
                    <p class="card-text text-left testoGestisciEventi"><span>Numero Biglietti: </span><?php echo $evento["Quantita"] ?></p>  
                    <p class="card-text text-left testoGestisciEventi"><span>Evento: </span><?php echo $evento["nome"] ?></p>                  
                    <p class="card-text text-left testoGestisciEventi"><span>Data Evento: </span><?php echo $evento["dataEvento"] ?></p> 
                    <p class="card-text text-left testoGestisciEventi"><span>Ora Evento: </span><?php echo substr($evento["Ora"], 0, 5)?></p>  
                    <p class="card-text text-left testoGestisciEventi"><span>Luogo Evento: </span><?php echo $evento["Luogo"] ?></p>
                  </div>
                  <div class="card-footer">
                    Codice Ordine: <?php echo $evento["codiceOridne"]?>
                  </div>
              </div>
        </div>
      <?php endforeach;?>
</div>