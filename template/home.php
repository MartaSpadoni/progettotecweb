 <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active">
        <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiSponsorizzati"][0]["Codice"] ?>"><img src="upload/<?php echo $templateParams["eventiSponsorizzati"][0]["immagine"]?>" alt="<?php echo $templateParams["eventiSponsorizzati"][0]["nome"]?>"></a>
        </div>      
        <div class="item">
        <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiSponsorizzati"][1]["Codice"] ?>"><img src="upload/<?php echo $templateParams["eventiSponsorizzati"][1]["immagine"]?>" alt="<?php echo $templateParams["eventiSponsorizzati"][1]["nome"]?>"></a>
        </div>
        <div class="item">
        <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiSponsorizzati"][2]["Codice"] ?>"><img src="upload/<?php echo $templateParams["eventiSponsorizzati"][2]["immagine"]?>" alt="<?php echo $templateParams["eventiSponsorizzati"][2]["nome"]?>"></a>
        </div>
      </div>

      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
  </div>
    
  <div class="container text-center"  >    
    <h3>Top Seller</h3><br>

    <div class="row text-center" id="TopSeller" >
        <div class="col-sm-3 col-xs-6" >
        <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiTop"][0]["Codice"] ?>"><img src="upload/<?php echo $templateParams["eventiTop"][0]["immagine"]?>" class="img-responsive" style="width:100%" alt="Image"></a>
          <p><?php echo $templateParams["eventiTop"][0]["nome"]?></p>
        </div>
        <div class="col-sm-3 col-xs-6"> 
        <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiTop"][1]["Codice"] ?>"><img src="upload/<?php echo $templateParams["eventiTop"][1]["immagine"]?>" class="img-responsive" style="width:100%" alt="Image"></a>
          <p><?php echo $templateParams["eventiTop"][1]["nome"]?></p>
        </div>
        <div class="col-sm-3" id="third"> 
        <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiTop"][2]["Codice"] ?>"><img src="upload/<?php echo $templateParams["eventiTop"][2]["immagine"]?>" class="img-responsive" style="width:100%" alt="Image"></a>
          <p><?php echo $templateParams["eventiTop"][2]["nome"]?></p>
        </div>
        <div class="col-sm-3"> 
        <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiTop"][3]["Codice"] ?>"><img src="upload/<?php echo $templateParams["eventiTop"][3]["immagine"]?>" class="img-responsive" style="width:100%" alt="Image"></a>
          <p><?php echo $templateParams["eventiTop"][3]["nome"]?></p>  
        </div>

    </div>
  </div><br>
  
  <div class="container text-center">
    <h3>Consigliati per te</h3><br>
    <div class="row consigliati">
        <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiConsigliati"][0]["Codice"] ?>"> <img src="upload/<?php echo $templateParams["eventiConsigliati"][0]["immagine"]?>" class="img-responsive" style="width:100%" alt="Image"></a>
        <p></p> 
    </div>
    <div class="row consigliati">
    <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiConsigliati"][1]["Codice"] ?>"><img src="upload/<?php echo $templateParams["eventiConsigliati"][1]["immagine"]?>" class="img-responsive" style="width:100%" alt="Image"></a>
      <p></p> 
    </div>
    <div class="row consigliati">
    <a href="dettaglioArtista.php?id=<?php echo $templateParams["eventiConsigliati"][2]["Codice"] ?>"><img src="upload/<?php echo $templateParams["eventiConsigliati"][2]["immagine"]?>" class="img-responsive" style="width:100%" alt="Image"></a>
      <p></p> 
    </div>
  </div>