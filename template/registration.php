<form class="form-horizontal" action='' method="POST">
  <fieldset>
    <h3 class="registration-heading">Registrazione</h3>

    <?php if(isset($templateParams["success"])):?>
        <div class="alert alert-success" id="successo">
          <p><?php echo $templateParams["success"] ?></p> 
          </br>
          <a id="successLogin" href="login.php">Login</a> 
        </div>
    <?php endif; ?>
    
    <?php if(isset($templateParams["failure"])): ?>
      <div class="alert alert-danger" id="insuccesso">
        <p><?php echo $templateParams["failure"] ?></p> 
      </div>
    <?php endif; ?>

    <hr>

    <div class="control-group">
      <!-- Nome e Cognome-->
      <label class="control-label"  for="nome">Nome</label>
      <div class="controls">
        <input type="text" id="nome" name="nome" placeholder="" class="input-xlarge" required>
      </div>
      </br>
      <label class="control-label"  for="cognome">Cognome</label>
      <div class="controls">
        <input type="text" id="cognome" name="cognome" placeholder="" class="input-xlarge" required>
      </div>
    </div>
    </br>
    <!-- Data Nascita e Indirizzo-->
    <div class="control-group-2">
      <label class="control-label"  for="data">Data di Nascita</label>
      <div class="controls">
        <input type="date" id="data" name="data" placeholder="" class="input-xlarge"  required>
      </div>
      </br>
      <label class="control-label"  for="indirizzo">Indirizzo</label>
      <div class="controls">
        <input type="text" id="indirizzo" name="indirizzo" placeholder="" class="input-xlarge"  required>
      </div>
    </div>
    </br>
    <!-- Cliente/Organizzatore -->
    <div class="custom-control custom-checkbox mb-3">
      <input type="checkbox" name="organizzatore" class="custom-control-input" id="organizzatore" onclick="showDesc()">
      <label class="custom-control-label" for="organizzatore">Organizzatore</label>
      <p id="p"></p>
    </div>

    <script >
      function showDesc() {
        if(document.getElementById("organizzatore").checked == true) {
          if(!document.getElementById("p").hasChildNodes()) {
            var text = document.createElement("TEXTAREA");
            text.id="text";
            text.placeholder="Descrizione";
            text.rows="5";
            text.cols="40";
            text.required;
            document.getElementById("p").appendChild(text);
          } else {
            $("#text").show();
          }
        } else {
          $("#text").hide();
        }
      }
    </script>

    <div class="control-group">
      <!-- E-mail -->
      <label class="control-label" for="email">E-mail</label>
      <div class="controls">
        <input type="text" id="email" name="email" placeholder="" class="input-xlarge" required>
      </div>
    </div>
    </br>
    <div class="control-group">
      <!-- Password-->
      <label class="control-label" for="password">Password</label>
      <div class="controls">
        <input type="password" id="password" name="p" placeholder="" class="input-xlarge" required>
        <p class="help-block">La password deve essere di almeno 8 caratteri</p>
      </div>
    </div>
    </br>
    <div class="control-group">
      <!-- Registrati -->
      <div class="controls">
        <button  class="btn-success" id="registrati" onclick="onSubmit(this.form, this.form.password)" >Registrati</button>
      </div>
    </div>
  </fieldset>
</form>

<script>
  function onSubmit(form, password) {
    if($("#password").val().length >= 8) {
      var p = document.createElement("input");
      // Aggiungi un nuovo elemento al tuo form.
      form.appendChild(p);
      p.name = "p";
      p.type = "hidden"
      p.value = hex_sha512(password.value);
      // Assicurati che la password non venga inviata in chiaro.
      password.value = "";
      form.submit();
    } 
  }
</script>