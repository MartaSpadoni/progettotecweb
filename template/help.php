<div class="container">
  <h3 id="titoloMieiBiglietti">Help & Contatti</h3>
  <p>In caso di problemi o perplessità non esitare a contattare un organizzatore. In seguito troverai la lista di tutti gli Organizzatori di eventi a noi affiliati, non esitare a contattarli.</p>
    <?php foreach( $templateParams["amministratori"] as $amministratore):?>
        <div class="row">
            <div class="card text-center" id="OrganizzatorCard">
                <div class="card-body">
                 <h5 class="card-title" id="titoloGestisciOrganizzatoreStampato"><?php echo $amministratore["Nome"]." ".$amministratore["Cognome"]?></h5>
                 <p class="card-text">Per qualsiasi informazione aggiuntiva contattami alla mia email: </p>
                </div>
                  <div class="card-footer text-muted">
                      <p><?php echo $amministratore["Email"]?></p>
                    </div>
            </div>
        </div>
    <?php endforeach;?>
</div>