<div class="container">
  <h2 id="miei eventi">I miei eventi</h2>
    <?php if(isset($templateParams["success"])):?>
        <div class="alert alert-success" id="successo">
            <p><?php echo $templateParams["success"] ?></p> 
        </div>
    <?php endif; ?>
    
    <?php if(isset($templateParams["failure"])): ?>
        <div class="alert alert-danger" id="insuccesso">
            <p><?php echo $templateParams["failure"] ?></p> 
        </div>
    <?php endif; ?>
    <?php if(!empty($templateParams["eventi"])):?>
    <?php foreach( $templateParams["eventi"] as $evento):?>
    <div class="row">
     <div class="container"> 
                <div class="card"  id="eventoCard">
                <!-- <div class="col-xs-8"> -->
                  <img class="img-responsive" src="upload/<?php echo $evento["NomeImmagine"] ?>" alt="Image" style="width:100%">
                <!-- </div> -->
                </div>
                <div class="card-header">
                    <h3 class="card-title" style="display:inline"id="titoloEvento"><?php echo $evento["Nome"]?></h3>
                     <?php if(!$evento["Sponsorizzato"]):?>
                        <a  href="mieiEventi.php?sponsorizza=<?php echo $evento["Codice"]?>" class="btn btn-primary" style=" position: relative; float: right;" id="sponsorizza" >Sponsorizza</a>
                    <?php endif; ?>
                    <p class="card-text"><?php echo $evento["DescrLunga"]?></p>
                </div>
                    <div class="card-body">
                        <div class="table-responsive" id="dettaglio">
                            <table class="table table-hover" id="table">
                            <thead>
                            <tr>
                            <th>Data</th>
                            <th>Ora</th>
                            <th>Luogo</th>
                            <th>Biglietti totali</th>
                            <th>Biglietti disponibili</th>
                            <th>Prezzo</th>
                            </tr>
                            </thead>
                            <tbody class="text-center"> 
                                <?php foreach($templateParams[$evento["Nome"]] as $dettaglio):?> 
                                <tr>
                                    <td> <?php echo $dettaglio["Data"]?> </td>
                                    <td> <?php echo $dettaglio["Ora"]?> </td>
                                    <td> <?php echo $dettaglio["Luogo"]?> </td>
                                    <td> <?php echo $dettaglio["BigliettiTotali"]?> </td>
                                    <td> <?php echo $dettaglio["BigliettiDisponibili"]?> </td>
                                    <td> <?php echo $dettaglio["Prezzo"]?> </td>
                                    <td><button id="elimina"  onclick=sendNotification(<?php echo $dettaglio["Codice"];?>) for="elimina">Elimina</button></td>
                                </tr>
                                <?php endforeach;?> 
                            </tbody>
                            </table> 
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        <button class="btn btn-primary" onclick=sendNot(<?php echo $evento["Codice"];?>)>Elimina Evento</button>
                    </div>
        </div>
    </div>
      <?php endforeach;?>
      <?php else: echo "Non hai ancora creato eventi" ?>
      <?php endif;?>
</div>

<script>
    function sendNotification(cod) {
        $.post("sendNotification.php", {type:2, codDettaglio:cod}, function(data) {
            if(data){
                window.location.assign("mieiEventi.php?id="+cod);
            }
        });
    }
    function sendNot(cod) {
        $.post("sendNotification.php", {type:3, codEvento:cod});
        window.location.assign("mieiEventi.php?evento="+cod);
    }
</script>