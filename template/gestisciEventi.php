<div class="container">
  <h3 id="titoloGestisciEventi">Tutti gli eventi</h3>
      <?php foreach( $templateParams["eventi"] as $evento):?>
        <div class="row">
              <div class="card text-center"  id="eventoCard">
                  <img  src="upload/<?php echo $evento["NomeImmagine"] ?>" class="card-img-top immaginiBiglietti"  alt="Image" >
                    <div class="card-header">
                      <h5 class="card-title" id="titoloGestisciEventoStampato"><?php echo $evento["nome"]?></h5>
                    </div>
                 <div class="card-body">
                    <p class="card-text text-left testoGestisciEventi"><span>Descrizione: </span><?php echo $evento["DescrLunga"]?></p>
                    <p class="card-text text-left testoGestisciEventi"><span>Categoria: </span><?php echo$evento["nomeCategoria"]?></p>
                    <p class="card-text text-left testoGestisciEventi"><span>Organizzatore: </span><?php echo $evento["nomeOrganizzatore"] ?></p>
                      <button class="btn btn-primary" onclick=sendNotification(<?php echo $evento["Codice"]?>)>Elimina Evento</button>
                  </div>
              </div>
        </div>
      <?php endforeach;?>
</div>

<script>
  function sendNotification(cod) {
    $.post("sendNotification.php", {type:4, codEvento:cod}, function(data) {
           window.location.assign("gestisciEventi.php?evento="+cod);
        });
  }
</script>