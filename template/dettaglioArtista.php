<div class="container-fluid dettArt">
<div class="container">
    <div class="row" id="rowImgDettaglio">

      <img class="img-responsive" src="upload/<?php echo $templateParams["evento"]["immagine"]?>" alt="">
      <div class="centered-t"><?php echo $templateParams["evento"]["DescrLunga"]?></div>
    </div>
  </div>
</div>
<div class="container">
<div class="table-responsive">
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Luogo</th>
        <th>Data</th>
        <th>Ora</th>
        <th>Prezzo</th>
        <th> Biglietti </th>
      </tr>
    </thead>
    <tbody class="text-center">
      <?php foreach($templateParams["DettaglioEvento"] as $e):?>
        <tr>
        <td><div><?php echo $e["Luogo"] ?></div></td>
        <td><?php echo date("d/m/Y", strtotime($e["Data"])) ?></td>
        <td><?php echo substr($e["Ora"],0,5);?></td>
        <td><?php echo $e["Prezzo"] ?></td>
        <!-- metteremo il link al carrello con l'id del dettaglio evento così da poter acquistare più biglietti -->
        <?php if($e["BigliettiDisponibili"] > 0): ?>
        <td><a href="dettaglioAcquisto.php?dettaglioEvento=<?php echo $e["Codice"]?>"><img src="img/addCart.png" alt="Add to cart" class="tableImg"></a></td>
        <?php else: ?>
        <td style="color:red; font-weight: bold;"><span id="soldOut">SOLD OUT</span></td>
        <?php endif?>
      </tr>
      <?php endforeach;?>
    </tbody>
  </table>
</div>
</div>