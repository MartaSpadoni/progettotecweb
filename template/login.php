<div class="container" >
    <div class="row">
        <div class="col-md-6 col-lg-5 mx-auto">
            <h3 class="login-heading">Login</h3>
            <?php if(isset($templateParams["failure"])): ?>
                <div class="alert alert-danger" id="insuccesso">
                    <p><?php echo $templateParams["failure"] ?></p> 
                </div>
            <?php endif; ?>
            <hr>
            <form action="login.php" method="POST" name="login_form">
                <div class="form-label-group">
                <label for="email">Email</label>
                    <input type="email" id="email" class="form-control" name="email" placeholder="Email" required
                        autofocus>

                </div>
                </br>
                <div class="form-label-group">
                <label for="password">Password</label>
                    <input type="password" id="password" class="form-control" name="p" placeholder="Password" required>

                </div>
                </br>
                <input type="button" id="Login" class="btn btn-lg btn-primary btn-block btn-login text-uppercase font-weight-bold mb-2" value="Accedi" onclick="onSubmit(this.form, this.form.password);" >
                </br>
                <div class="text-center">
                    <a class="small" href="registration.php">Registrati</a>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
  function onSubmit(form, password) {
      var p = document.createElement("input");
      // Aggiungi un nuovo elemento al tuo form.
      form.appendChild(p);
      p.name = "p";
      p.type = "hidden"
      p.value = hex_sha512(password.value);
      // Assicurati che la password non venga inviata in chiaro.
      password.value = "";
      form.submit();
  }
</script>