<?php
require_once 'connection.php';
//notifiche
if(isset($_SESSION["idUtente"])){
   $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }
//Base Template
$templateParams["titolo"] = "JumpTheLine - ".$_GET["nome"];
$templateParams["pagina"] = "categoria.php";
$templateParams["nomecategoria"]= $_GET["nome"];
//prendo eventi 

$templateParams["eventicategoria"]= $dbh->getCategoriesEvents($_GET["idcategoria"]);

require 'template/base.php';
?>