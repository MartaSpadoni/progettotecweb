<?php
require_once 'connection.php';  
switch($_POST["type"]){
   case 1: 
      $dbh->sendNotification($dbh-> getOrganiserEventDetails($_POST["codiceEvento"])["CodiceOrganizzatore"], $_SESSION["idUtente"], "Conferma Acquisto" ,"La ringraziamo per aver acquistato ".$_POST["numBiglietti"]." biglietti dal nostro sito!", $_POST["codiceEvento"]);
   break;
   case 2:
      $users = $dbh->getUserToNotify($_POST["codDettaglio"]);
      foreach($users as $u){
         $dbh->sendNotification($dbh-> getOrganiserEventDetails($_POST["codDettaglio"])["CodiceOrganizzatore"], $u["CodUtente"], "Eliminazione data Evento" ,"Ci scusiamo per il disguido ma la data dell'evento è stata annullata", $_POST["codDettaglio"]);
      }
      echo true;
   break;
   case 3:
      $details=$dbh->getEventDetails($_POST["codEvento"]);
      foreach($details as $d){
         $users = $dbh->getUserToNotify($d["Codice"]);
         foreach($users as $u){
            $dbh->sendNotification($dbh-> getOrganiserEventDetails($d["Codice"])["CodiceOrganizzatore"], $u["CodUtente"], "Evento annullato" ,"Ci scusiamo per il disguido ma l'evento è stato annullato", $d["Codice"]);
         }
      }
   break;
   case 4:
      $details=$dbh->getEventDetails($_POST["codEvento"]);
      foreach($details as $d){
         $users = $dbh->getUserToNotify($d["Codice"]);
         foreach($users as $u){
            $dbh->sendNotification($_SESSION["idUtente"], $u["CodUtente"], "Evento annullato" ,"Ci scusiamo per il disagio ma l'evento è stato annullato dall'amministratore in quanto non conforme alle nostre politiche", $d["Codice"]);
         }
      }
   $dbh->sendNotification($_SESSION["idUtente"], $dbh-> getOrganiserEventDetails($d["Codice"])["CodiceOrganizzatore"], "Evento annullato" ,"L'amministratore del sito ha provveduto ad eliminare l'evento, in quanto non conforme alle nostre politiche", $d["Codice"]);
   break;
   case 5:
      $eventi=$dbh->getEventForOrganizer($_POST["codOrganizzatore"]);
      foreach($eventi as $e){
         $details=$dbh->getEventDetails($e["Codice"]);
            foreach($details as $d){
               $users = $dbh->getUserToNotify($d["Codice"]);
                  foreach($users as $u){
                     $dbh->sendNotification($_SESSION["idUtente"], $u["CodUtente"], "Evento eliminato" ,"Ci scusiamo per il disagio ma l'evento è stato annullato dall'amministratore in quanto creato da un organizzatore non conforme alle nostre politiche. Non troverà più riferimenti al suo acquisto e riceverà al più presto un rimborso", $d["Codice"]);
                  }
           }
       }
   break;
   default:
}
?>
