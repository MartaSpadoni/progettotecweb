<?php
require_once 'connection.php';
//Base Template
$templateParams["titolo"] = "JumpTheLine - Home";
if(isset($_SESSION["tipo"]) && ($_SESSION["tipo"] == "organizzatore" || $_SESSION["tipo"] == "admin")){
   header("location: ./areaUtente.php");
}else{
   $templateParams["pagina"] = "home.php";
   $templateParams["eventiSponsorizzati"] = checkArrayLenght($dbh->getSponsorizedEvent(3), 3, $dbh);
   $topSeller=array();
   foreach($dbh->getIdTopSellerEvent() as $top){
      array_push($topSeller, $dbh->getEventById($top));
   }
   $templateParams["eventiTop"] = checkArrayLenght($topSeller, 4, $dbh);
   if(isset($_SESSION["idUtente"])){
      $templateParams["eventiConsigliati"] = checkArrayLenght($dbh->getUserRecommendedEvents($_SESSION["idUtente"]), 3, $dbh, $dbh->getCategoryUserRecommendedEvents($_SESSION["idUtente"]));
      $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
      $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
       if($numeroNotifiche > 0){
          $templateParams["numeroNotifiche"] = $numeroNotifiche;
       }else{
          $templateParams["numeroNotifiche"]="";
       }
    }else{
      $templateParams["eventiConsigliati"] = $dbh->getRandomEvent(3);
       $templateParams["numeroNotifiche"] = "";
    }
}

function checkArrayLenght($a, $n, $dbh, $cat=NULL){
   if(count($a) < $n){
         $a= array_merge($a, $dbh->getRandomEvent($n, $cat));
   }
   return $a;
}

require 'template/base.php';
?>