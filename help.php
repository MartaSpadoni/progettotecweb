<?php
require_once 'connection.php';
//notifiche
if(isset($_SESSION["idUtente"])){
   $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }
//Base Template
$templateParams["titolo"] = "JumpTheLine - Help & Contatti";
$templateParams["pagina"] = "help.php";

$templateParams["amministratori"]=$dbh->getAdmin();

require 'template/base.php';
?>