function formhash(form, password) {
    // Crea un elemento di input che verrà usato come campo di output per la password criptata.
    var p = document.createElement("input");
    // Aggiungi un nuovo elemento al tuo form.
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden"
    p.value = hex_sha512(password.value);
    // Assicurati che la password non venga inviata in chiaro.
    password.value = "";
    // Come ultimo passaggio, esegui il 'submit' del form.
    form.submit();
}

function addRow() {
    var table = document.getElementById("table");
    var tbody = table.getElementsByTagName('tbody')[0];
    var colonne = table.getElementsByTagName('th').length;
    var riga = table.getElementsByTagName('tr').length; 
    var tr = document.createElement('tr');
    for(var i=0; i<colonne; i++){
        var td = document.createElement('td');
        var input = document.createElement('input');
        switch(i) {
            case 0:
                input.id = "data";
                input.name = "data"+riga;
                input.type = "date";
                break;
            case 1:
                input.id = "ora";
                input.name = "ora"+riga;
                input.type = "time";
                break;
            case 2:
                input.id = "luogo";
                input.name = "luogo"+riga;
                input.type = "text";
                break;
            case 3:
                input.id = "biglietti";
                input.name = "biglietti"+riga;
                input.type = "number";
                input.min="0";
                break;
            case 4:
                input.id = "prezzo";
                input.name = "prezzo"+riga;
                input.type = "number";
                input.step = "0.10";
                input.min="0";
                break;
            default:
                break;
        }
        td.appendChild(input);
        tr.appendChild(td);
    }
    tbody.appendChild(tr);
}           