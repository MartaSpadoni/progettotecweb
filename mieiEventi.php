<?php
require_once 'connection.php';
//Base Template
$templateParams["titolo"] = "JumpTheLine ";
$templateParams["pagina"] = "mieiEventi.php";

if(isset($_SESSION["idUtente"])){
  $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }

    if(isset($_GET["evento"])) {
      $result = $dbh->removeEvent($_GET["evento"]);
      if($result) {
        $templateParams["success"] = "La cancellazione è stata completata con successo!";
      } else {
        $templateParams["failure"] = "La cancellazione non è andata a buon fine";
      }
    }

    if(isset($_GET["id"])) {
      $codiceEvento = $dbh->getEventDetailsById($_GET["id"])["CodiceEvento"];
      $num = $dbh->getEventDetails($codiceEvento);
      if(sizeOf($num)==1) {
        $result = $dbh->removeEvent($codiceEvento);
        if($result) {
          $templateParams["success"] = "La cancellazione è stata completata con successo!";
        } else {
          $templateParams["failure"] = "La cancellazione non è andata a buon fine";
        }
      } else {
        $result = $dbh->deleteDetail($_GET["id"]);
        if($result) { 
          $templateParams["success"] = "La cancellazione è stata completata con successo!";
        } else {
          $templateParams["failure"] = "La cancellazione non è andata a buon fine";
        }
      }
    }

    if(isset($_GET["sponsorizza"])) {
      $result = $dbh->sponsorEvent($_GET["sponsorizza"]);
      if($result) { 
        $templateParams["success"] = "Evento sponsorizzato!";
      } else {
        $templateParams["failure"] = "L'operazione non è andata a buon fine";
      }
    }

    $templateParams["eventi"]= $dbh->getEventByOrganizer($_SESSION["idUtente"]);

    foreach ($templateParams["eventi"] as $evento) {
        $nome = $evento["Nome"];
        $templateParams[$nome] = $dbh->getEventDetails($evento["Codice"]);
    }

}else{
    $templateParams["numeroNotifiche"] = "";
    $templateParams["eventi"] = " ";
}

require 'template/base.php';
?>