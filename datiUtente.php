<?php
require_once 'connection.php';
//Base Template
$templateParams["titolo"] = "JumpTheLine ";
$templateParams["pagina"] = "datiUtente.php";
if(isset($_SESSION["idUtente"])){
  $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
  $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
  if($numeroNotifiche > 0){
     $templateParams["numeroNotifiche"] = $numeroNotifiche;
  }else{
     $templateParams["numeroNotifiche"]="";
  }
}else{
  $templateParams["numeroNotifiche"] = "";
}
if(isset($_SESSION['email'])){
    $dati = $dbh-> getUserByEmail($_SESSION["email"]);
    if(!empty($dati)){
       $templateParams["datiUtente"] = $dati;
    }else{
       $templateParams["datiUtente"]="";
    }

    if(!empty($_POST["nome"])) {
      $dbh->modifyUser("Nome", $_POST["nome"], $_SESSION['email']);
      header("location: datiUtente.php");
    } 
    if(!empty($_POST["cognome"])) {
      $dbh->modifyUser("Cognome", $_POST["cognome"], $_SESSION['email']);
      header("location: datiUtente.php");
    }
    if(!empty($_POST["data"])) {
      $dbh->modifyUser("DataNascita", $_POST["data"], $_SESSION['email']);
      header("location: datiUtente.php");
    }
    if(!empty($_POST["ind"])) { 
      $dbh->modifyUser("Indirizzo", $_POST["ind"], $_SESSION['email']);
      header("location: datiUtente.php");
    }
    if(!empty($_POST["password"])) {  
      $password = $_POST['password'];
      if(strlen($password)>=8) {
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));  
        $password = hash('sha512', $password.$random_salt);
        $result = $dbh->modifyPasswordUser($password, $random_salt, $_SESSION['email']);
        if($result) {
          $msg = "La password è stata modificata con successo!";
        } else {
          $msg = "La modifica della password non è andata a buon fine";
        }
      } else {
        $msg = "Operazione fallita. La password deve essere di almeno 8 caratteri";
      }
      header("location: datiUtente.php?messaggio=".$msg);
    }
} else {
   $templateParams["datiUtente"]="";
}

require 'template/base.php';
?>