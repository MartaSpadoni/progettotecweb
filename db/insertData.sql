INSERT INTO `Utente` (`Codice`, `Email`, `Password`, `Nome`, `Cognome`, `DataNascita`, `Indirizzo`, `Tipo`) VALUES
        (1, 'lucia.sternini2@studio.unibo.it', ' ', 'Lucia', 'Sternini', '1998-02-09', 'Via Rossi, 6','admin'),
        (2, 'marta.spadoni5@studio.unibo.it', ' ', 'Marta', 'Spadoni', '1998-06-20', 'Via Verdi, 3', 'admin'),
        (3, 'andrea.rettaroli@studio.unibo.it',' ', 'Andrea','Rettaroli','1997-04-25','Via Bianchi, 8','admin');

INSERT INTO `Categoria` (`Codice`, `Nome`) VALUES
        (1, 'Concerti'),
        (2, 'Sport'),
        (3, 'Musei'),
        (4, 'Teatro');
        
INSERT INTO `Evento`( `Nome`,`DescrLunga`,`DescrBreve`,`CodiceCategoria`,`CodiceOrganizzatore`,`NomeImmagine`)VALUES
('Ultimo','Tutto inizia con la vittoria nella categoria giovani alla 68esima edizione del Festival di Sanremo con “Il ballo delle incertezze”. Un successo ottenuto gradualmente e che ha conquistato il pubblico a suon di hit. I tre album “Pianeti”, “Peter Pan” e “Colpa delle favole” continuano a registrare numeri da record.',
'Tutto pronto per "La Favola", il concerto evento di Ultimo.',1,1,'ultimo.jpg'),
('Tiziano Ferro',`Tiziano Ferro è nato a Latina il 21 febbraio 1980. Nel corso della sua carriera ha cantato anche in spagnolo, inglese, francese e portoghese. Ha venduto oltre 15 milioni di copie nel mondo, prevalentemente in Europa e in America Latina, e nella sua carriera ventennale ha ottenuto numerosi premi, candidature e riconoscimenti tra i più importanti a livello nazionale e internazionale.`,
'Tiziano Ferro (Latina, 21 febbraio 1980) è un cantautore, produttore discografico e doppiatore italiano.',1,1,'tizianoferro.jpg'),
('Achille Lauro','Dopo il successo del suo ultimo album, Lauro calca i palchi di tutta Italia con un tour lungo più di un anno, che lo porta a Roma sul palco del Circo Massimo, per lo storico concerto di Capodanno e lo lancia verso la sua prossima avventura: il Festival di Sanremo 2019.',
'Achille Lauro, pseudonimo di Lauro De Marinis (Roma, 11 luglio 1990), è un rapper italiano.',1,1,'achillelauro.jpg'),
('Juventus-Inter','Non è una partita qualsiasi e questo più che un luogo comune è un assioma, quello che in matematica è un principio evidente e non deve essere dimostrato. Il Derby d’Italia.',
'Le due squadre si affrontano in un match che si preannuncia spettacolare.',2,1,'juventus-inter.jpg'),
('Juventus-Milan','Uno scontro che si preannuncia infuocato, il Milan reduce da due sconfitte affronta la Juventus capolista.',
'Le due squadre si affrontano in un match che si preannuncia spettacolare.',2,1,'juventus-milan.jpg'),
('Inter-Milan','Il Derby più atteso in Italia. Di che colori vestirà Milano stasera?',
'Le due squadre si affrontano in un match che si preannuncia spettacolare.',2,1,'inter-milan.jpg'),
('Lo schiaccianoci',`È mezzanotte, e tutto intorno a lei inizia a crescere: la sala, l'albero di Natale, i giocattoli e una miriade di topi che cercano di rubarle lo schiaccianoci. Clara tenta di cacciarli, quando lo Schiaccianoci si anima e partecipa alla battaglia con i soldatini di Fritz: alla fine, rimangono lui e il Re Topo, che lo mette in difficoltà.`,
'Lo spettacolo più atteso per il Natale torna nei maggiori teatri Italiani.',4,1,'loschiaccianoci.jpg'),
('Lago dei cigni',`Il lago dei cigni, oggi forse il balletto più famoso del mondo, continua a mantenere intatto tutto il suo fascino per l'atmosfera lunare che accompagna l'apparizione di Odette, per il doppio ruolo di Odette-Odile, cigno bianco e cigno nero, per l'eterna lotta fra il Bene e il Male.`,
'Il balletto più famoso del mondo.',4,1,'lagodeicigni.jpg'),
('Impressionisti segreti',`E' arrivata la mostra "Impressionisti Segreti" con oltre 50 opere dei più grandi artisti impressionisti, tra cui Monet, Renoir, Cézanne, Pissarro, Sisley, Caillebotte, Morisot, Gonzalès, Gauguin e Signac.`,'Mostra impressionisti segreti al Palazzo Bonaparte di Roma',3,1,'impressionisti.jpg'),
('Mostra di pittura','Dal Duecento al Novecento un compendio di capolavori assoluti della pittura occidentale','Mostra alle Gallerie degli Uffizi',3,1,'uffizi.jpg');
INSERT INTO `DettaglioEvento` (`Data`, `Ora`, `Luogo`, `BigliettiTotali`, `BigliettiDisponibili`, `Prezzo`, `CodiceEvento`) VALUES 
('2020-03-21', '20:45:00', 'Milano', 100, 100, 10.00 , 1),
('2020-03-30', '20:45:00', 'Roma', 200, 200, 10.00 , 1),
('2020-04-30', '20:45:00', 'Firenze', 200, 200, 10.00 , 1),
('2020-04-21', '21:45:00', 'Torino', 100, 100, 10.00 , 1),
('2020-04-10', '21:45:00', 'Verona', 200, 200, 10.00 , 1),
('2020-05-11', '21:45:00', 'Genova', 200, 200, 10.00 , 1),
('2020-04-21', '21:45:00', 'Torino', 100, 100, 10.00 , 2),
('2020-04-10', '21:45:00', 'Verona', 200, 200, 10.00 , 2),
('2020-05-11', '21:45:00', 'Genova', 200, 200, 10.00 , 2),
('2020-03-21', '21:45:00', 'Milano', 100, 100, 10.00 , 3),
('2020-03-10', '21:45:00', 'Verona', 200, 200, 10.00 , 3),
('2020-04-11', '21:45:00', 'Roma', 200, 200, 10.00 , 3),
('2020-03-15', '20:45:00', 'Torino Juventus Stadium', 1000, 1000, 20.00 , 4),
('2020-04-15', '20:45:00', 'Torino Juventus Stadium', 1000, 1000, 20.00 , 5),
('2020-04-15', '20:45:00', 'Milano San Siro', 1000, 1000, 20.00 , 6),
('2020-03-15', '20:45:00', 'Milano', 300, 300, 15.00 , 7),
('2020-03-20', '20:45:00', 'Roma', 300, 300, 15.00 , 7),
('2020-03-25', '20:45:00', 'Bologna', 300, 300, 15.00 , 7),
('2020-03-15', '20:45:00', 'Milano', 300, 300, 15.00 , 8),
('2020-03-20', '20:45:00', 'Roma', 300, 300, 15.00 , 8),
('2020-03-25', '20:45:00', 'Bologna', 300, 300, 15.00 , 8),
('2020-05-25', '09:30:00', 'Roma', 300, 300, 15.00 , 9),
('2020-05-26', '09:30:00', 'Roma', 300, 300, 15.00 , 9),
('2020-05-27', '09:30:00', 'Roma', 300, 300, 15.00 , 9),
('2020-05-28', '09:30:00', 'Roma', 300, 300, 15.00 , 9),
('2020-04-25', '09:30:00', 'Firenze', 300, 300, 15.00 , 10),
('2020-04-26', '09:30:00', 'Firenze', 300, 300, 15.00 , 10),
('2020-04-27', '09:30:00', 'Firenze', 300, 300, 15.00 , 10),
('2020-04-28', '09:30:00', 'Firenze', 300, 300, 15.00 , 10);







