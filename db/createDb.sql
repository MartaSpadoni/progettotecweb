--
-- Database: `jumptheline`
--
CREATE DATABASE IF NOT EXISTS `jumptheline` DEFAULT CHARACTER SET utf8 ;
USE `jumptheline` ;

--
-- Table structure for table `categoria`
--

CREATE TABLE IF NOT EXISTS `Categoria` (
  `Codice` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(20) NOT NULL,
  PRIMARY KEY (`Codice`)
) ENGINE=InnoDB;

--
-- Table structure for table `utente`
--

CREATE TABLE IF NOT EXISTS `Utente` (
  `Codice` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(20) NOT NULL,
  `Cognome` varchar(20) NOT NULL,
  `Sesso` char(1) NOT NULL,
  `DataNascita` date NOT NULL,
  `Indirizzo` varchar(30) NOT NULL,
  `Descrizione` varchar(200) NULL, /*solo se è organizzatore*/
  `Email` varchar(50) NOT NULL UNIQUE,
  `Password` char(128) NOT NULL,
  `Salt` char(128) NOT NULL,
  `Tipo` varchar(20) NOT NULL, /*organizzatore, cliente, amministratore*/
  `Attivo` TINYINT NULL DEFAULT 0,
  PRIMARY KEY (`Codice`)
) ENGINE=InnoDB;

--
-- Table structure for table `evento`
--


CREATE TABLE IF NOT EXISTS `Evento` (
  `Codice` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL,
  `DescrLunga` varchar(200) NOT NULL,
  `DescrBreve` varchar(100) NOT NULL,
  `StatoApprovazione` varchar(20) NOT NULL,
  `CodiceCategoria` int(11) NOT NULL,
  `CodiceOrganizzatore` int(11) NOT NULL,
  PRIMARY KEY (`Codice`),
  INDEX `fk_categoria_idx` (`CodiceCategoria` ASC),
  INDEX `fk_utente_idx` (`CodiceOrganizzatore` ASC),
  CONSTRAINT `fk_categoria`
    FOREIGN KEY (`CodiceCategoria`)
    REFERENCES `jumptheline`.`Categoria` (`Codice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_utente`
    FOREIGN KEY (`CodiceOrganizzatore`)
    REFERENCES `jumptheline`.`utente` (`Codice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;

--
-- Table structure for table `dettaglioEvento`
--

CREATE TABLE IF NOT EXISTS `DettaglioEvento` (
  `Codice` int(11) NOT NULL AUTO_INCREMENT,
  `CodiceEvento` int(11) NOT NULL,
  `Luogo` VARCHAR(200) NOT NULL,
  `Data` DATE NOT NULL,
  `Ora` TIME NOT NULL,
  `BigliettiTotali` int(11) NOT NULL,
  `BigliettiDisponibili` int(11),
  PRIMARY KEY (`Codice`),
  INDEX `fk_evento_idx` (`CodiceEvento` ASC),
  CONSTRAINT `fk_evento`
    FOREIGN KEY (`CodiceEvento`)
    REFERENCES `jumptheline`.`Evento` (`Codice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;

--
-- Table structure for table `Notifica`
--

CREATE TABLE IF NOT EXISTS `Notifica` (
  `Codice` int(11) NOT NULL AUTO_INCREMENT,
  `Mittente` int(11) NOT NULL,
  `Destinatario` int(11) NOT NULL,
  `Data` DATE NOT NULL,
  `Messaggio` varchar(300),
  `CodDettaglioEvento` int(11) NOT NULL,
  `Vista` boolean NOT NULL,
  PRIMARY KEY (`Codice`),
  CONSTRAINT `fk_mittente`
    FOREIGN KEY (`Mittente`)
    REFERENCES `jumptheline`.`Utente` (`Codice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_destinatario`
    FOREIGN KEY (`Destinatario`)
    REFERENCES `jumptheline`.`Utente` (`Codice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_dettaglio_evento`
    FOREIGN KEY (`CodDettaglioEvento`)
    REFERENCES `jumptheline`.`DettaglioEvento` (`Codice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;

--
-- Table structure for table `ordine`
--

CREATE TABLE IF NOT EXISTS `Ordine` (
  `Codice` int(11) NOT NULL AUTO_INCREMENT,
  `CodDettaglioEvento` int(11) NOT NULL,
  `Quantita` varchar(30) NOT NULL,
  `Data` date NOT NULL,
  PRIMARY KEY (`Codice`),
  CONSTRAINT `fk_det_evento`
    FOREIGN KEY (`CodDettaglioEvento`)
    REFERENCES `jumptheline`.`DettaglioEvento` (`Codice`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB;

/*Modifiche al db*/
ALTER TABLE `Evento` ADD `NomeImmagine` varchar(200) NOT NULL;

ALTER TABLE `Evento` ADD `Sponsorizzato` BOOLEAN NOT NULL DEFAULT FALSE;

ALTER TABLE `Evento` DROP `StatoApprovazione`;

ALTER TABLE `Utente` DROP `Attivo`;

ALTER TABLE `Utente` DROP `Sesso`;

ALTER TABLE `DettaglioEvento` ADD `Prezzo` DOUBLE(11,2) NOT NULL;

ALTER TABLE `ordine` CHANGE `Data` `Data` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `ordine` ADD CodUtente int(11) NOT NULL;

ALTER TABLE `notifica` CHANGE `Data` `Data` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE `notifica` ADD `Titolo` VARCHAR(50) NOT NULL ;

/*DA QUI*/

ALTER TABLE `evento` DROP CONSTRAINT `fk_utente`;

ALTER TABLE `evento` ADD CONSTRAINT `fk_utente`
FOREIGN KEY (`CodiceOrganizzatore`) references `Utente` (`Codice`) 
ON DELETE CASCADE;

ALTER TABLE `dettaglioevento` DROP CONSTRAINT `fk_evento`;

ALTER TABLE `dettaglioevento` ADD CONSTRAINT `fk_evento`
FOREIGN KEY (`CodiceEvento`) references `Evento` (`Codice`) 
ON DELETE CASCADE;

ALTER TABLE `ordine` DROP CONSTRAINT `fk_det_evento`;

ALTER TABLE `ordine` ADD CONSTRAINT `fk_det_evento` 
FOREIGN KEY (`CodDettaglioEvento`) references `DettaglioEvento` (`Codice`) 
ON DELETE CASCADE;

ALTER TABLE `notifica` DROP CONSTRAINT `fk_mittente`;

ALTER TABLE `notifica` DROP CONSTRAINT `fk_destinatario`;

ALTER TABLE `notifica` DROP CONSTRAINT `fk_dettaglio_evento`;

/*ALTER TABLE `notifica` ADD CONSTRAINT `fk_mittente`
FOREIGN KEY (`Mittente`) references `Utente` (`Codice`) 
ON DELETE CASCADE;

ALTER TABLE `notifica` ADD CONSTRAINT `fk_destinatario`
FOREIGN KEY (`Destinatario`) references `Utente` (`Codice`) 
ON DELETE CASCADE;

ALTER TABLE `notifica` ADD CONSTRAINT `fk_dettaglio_evento`
FOREIGN KEY (`CodDettaglioEvento`) references `DettaglioEvento` (`Codice`) 
ON DELETE CASCADE; */