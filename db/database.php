<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }

    public function getUserByEmail($email){
        $query = "SELECT Codice, Nome, Cognome, DataNascita, Indirizzo, Email, Salt, Tipo, Password FROM Utente WHERE Email=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$email);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result -> fetch_assoc();
    }

    public function getUserById($cod){
        $query = "SELECT Codice, Nome, Cognome, DataNascita, Indirizzo, Email, Tipo FROM Utente WHERE Codice=?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$cod);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result -> fetch_assoc();
    }

    public function criptPassAdmin($codice, $password) {
        $password = hex_sha512($password);
        $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));
        $password = hash('sha512', $password.$random_salt);
        $query = "UPDATE Utente SET Password = ?, Salt = ? WHERE Codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssi',$password, $random_salt, $codice);
        return $stmt->execute();
    }

    public function getEvent($nome){
        $query = "SELECT Codice, nome, DescrLunga, DescrBreve, codiceCategoria, codiceOrganizzatore, NomeImmagine as immagine FROM Evento WHERE Nome = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$nome);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function sponsorEvent($codEvento) {
        $query = "UPDATE Evento SET Sponsorizzato = '1' WHERE Codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$codEvento);
        return $stmt->execute();
    }

    public function getEventByOrganizer($id) {
        $query = "SELECT Codice, Nome, DescrBreve,DescrLunga, NomeImmagine, Sponsorizzato FROM Evento WHERE CodiceOrganizzatore = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRandomEvent($n, $categoria=NULL){
        $query = "SELECT Codice, nome, DescrLunga, DescrBreve, codiceCategoria, codiceOrganizzatore, NomeImmagine as immagine FROM Evento ";
        $end = "ORDER BY RAND() LIMIT ?";
        if(isset($categoria)){
            $query = $query."WHERE codiceCategoria != ? ".$end;
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('ii', $categoria, $n);
        }else{
            $query = $query.$end;
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('i',$n);
        }
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getSponsorizedEvent($n){
        $query = "SELECT Codice, nome, DescrLunga, DescrBreve, codiceCategoria, codiceOrganizzatore, NomeImmagine as immagine FROM Evento WHERE sponsorizzato = 1 ORDER BY RAND() LIMIT ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$n);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getTopSellerDetailsEvent(){
        $query = "SELECT codDettaglioEvento, COUNT(*) as numOrdini FROM ordine GROUP BY codDettaglioEvento ORDER BY numOrdini DESC";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    public function getIdTopSellerEvent(){
        $top = array();
        foreach($this->getTopSellerDetailsEvent() as $d){
            $query = "SELECT CodiceEvento FROM dettaglioevento WHERE codice = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param('i',$d["codDettaglioEvento"]);
            $stmt->execute();
            $result = $stmt->get_result();
           array_push($top, $result->fetch_assoc()["CodiceEvento"]);
        }
        return array_unique($top);
    }

    public function getUserRecommendedEvents($user){
        $ordini=$this->myOrder($user);
        $categorie = array();
        foreach($ordini as $o){
            array_push($categorie, $this->getEventById($o["codiceEvento"])["codiceCategoria"]);
        }
        if(count($categorie) == 0){
            return array();
        }
        return $this->getCategoriesEvents($categorie[0]);
        
    }

    public function getCategoryUserRecommendedEvents($user){
        $ordini=$this->myOrder($user);
        $categorie = array();
        foreach($ordini as $o){
            array_push($categorie, $this->getEventById($o["codiceEvento"])["codiceCategoria"]);
        }
        if(count($categorie) == 0){
            return NULL;
        }
        return $categorie[0];
        
    }

    public function getEventById($id){
        $query = "SELECT Codice, nome, DescrLunga, DescrBreve, codiceCategoria, codiceOrganizzatore, NomeImmagine as immagine FROM Evento WHERE codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function getEventDetails($id){
        $query = "SELECT Codice, Luogo, Data, Ora, BigliettiTotali, BigliettiDisponibili, Prezzo FROM dettaglioevento WHERE CodiceEvento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    public function getEventDetailsById($id){
        $query = "SELECT Codice, CodiceEvento, Luogo, Data, Ora, BigliettiTotali, BigliettiDisponibili, Prezzo FROM dettaglioevento WHERE Codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function deleteDetail($id) {
        $query = "DELETE FROM `dettaglioevento` WHERE Codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$id);
        return $stmt->execute();
    }

    public function getOrganiserEventDetails($cod){
        $query = "SELECT evento.CodiceOrganizzatore FROM dettaglioevento, evento WHERE evento.Codice=dettaglioevento.CodiceEvento AND dettaglioevento.Codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$cod);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function getCategories(){
        $query = "SELECT Codice, Nome FROM Categoria";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCategory($nome){
        $query = "SELECT Codice FROM Categoria WHERE Nome = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$nome);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function insertEvent($nome, $desc_breve, $desc_lunga, $categoria, $organizzatore, $immagine) {
        $numCategoria = $this->getCategory($categoria)["Codice"];
        $stmt = $this->db->prepare("INSERT INTO Evento (Nome, DescrLunga, DescrBreve, CodiceCategoria, CodiceOrganizzatore, NomeImmagine) VALUES (?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('sssiis',$nome, $desc_lunga, $desc_breve, $numCategoria, $organizzatore, $immagine);
        return $stmt->execute();
    }

    public function insertDetail($data, $ora, $luogo, $biglietti, $evento, $prezzo) {
        $codiceEvento = $this->getEvent($evento)["Codice"];
        $stmt = $this->db->prepare("INSERT INTO DettaglioEvento (Data, Ora, Luogo, BigliettiTotali, BigliettiDisponibili, CodiceEvento, Prezzo) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('sssiiid',$data, $ora, $luogo, $biglietti, $biglietti, $codiceEvento, $prezzo);
        return $stmt->execute();
    }

    public function insertUser($nome, $cognome, $email, $tipo, $password, $salt, $data, $indirizzo){
        $stmt = $this->db->prepare("INSERT INTO Utente (Nome, Cognome, Email, Tipo, Password, Salt, DataNascita, Indirizzo) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param('ssssssss',$nome, $cognome, $email, $tipo, $password, $salt, $data, $indirizzo);
        return $stmt->execute();
    }

    public function modifyUser($campo, $dato, $email){
        $query = "UPDATE Utente SET $campo = ? WHERE Email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ss',$dato, $email);
        return $stmt->execute();
    }

    public function modifyPasswordUser($password, $random_salt, $email) {
        $query = "UPDATE Utente SET Password = ?, Salt = ? WHERE Email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sss',$password, $random_salt, $email);
        return $stmt->execute();
    }

    public function checkNotifications($codUtente){
        $query = "SELECT COUNT(*) as Num FROM notifica WHERE Destinatario = ? AND Vista = 0";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $codUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC)[0]["Num"];
    }

    public function getUnreadNotifications($codUtente){
        $query = "SELECT notifica.codice, nome, cognome, titolo, messaggio, codDettaglioEvento, data FROM notifica, utente WHERE Destinatario = ? AND mittente = utente.Codice AND Vista = 0";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $codUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getReadNotifications($codUtente){
        $query = "SELECT notifica.codice, nome, cognome, titolo, messaggio, codDettaglioEvento, data FROM notifica, utente WHERE Destinatario = ? AND mittente = utente.Codice AND Vista = 1";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $codUtente);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function setNotificationRead($codNotifica){
        $query = "UPDATE notifica SET vista = 1 WHERE Codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$codNotifica);
        return $stmt->execute();
    }

    public function sendNotification($mittente, $destinatario, $titolo, $testo, $codDettaglioEvento){
        $stmt = $this->db->prepare("INSERT INTO notifica (mittente, destinatario, titolo, messaggio, CodDettaglioEvento) VALUES (?, ?, ?, ?, ?)");
        $stmt->bind_param('iissi',$mittente, $destinatario, $titolo, $testo, $codDettaglioEvento);
        return $stmt->execute();
    }

    public function getUserToNotify($codDettaglio){
        $query = "SELECT CodUtente FROM ordine WHERE CodDettaglioEvento = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$codDettaglio);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function deleteAllUsersNotifications($codUtente){
        $query="DELETE FROM notifica WHERE destinatario=? AND vista = 1";
        $stmt=$this->db->prepare($query);
        $stmt->bind_param("i",$codUtente);
        return $stmt->execute();
    }
    public function deleteUsersNotifications($codUtente, $cod){
        $query="DELETE FROM notifica WHERE destinatario=? AND codice = ?";
        $stmt=$this->db->prepare($query);
        $stmt->bind_param("ii",$codUtente, $cod);
        return $stmt->execute();
    }
    /**ricerca dalla categoria */
    public function getCategoriesEvents($codicecategoria){
        $query = "SELECT Codice, nome, DescrLunga, DescrBreve, codiceCategoria, codiceOrganizzatore, NomeImmagine as immagine, Sponsorizzato FROM Evento WHERE CodiceCategoria=?";
        $stmt=$this->db->prepare($query);
        $stmt->bind_param("i",$codicecategoria);
        $stmt->execute();
        $result=$stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    /**ricerca di un evento */
    public function getEventsBySearch($search){
        $query="SELECT Codice, nome, DescrLunga, DescrBreve, codiceCategoria, codiceOrganizzatore, NomeImmagine, Sponsorizzato FROM Evento WHERE Nome LIKE ? ";
        $stmt=$this->db->prepare($query);
        $search="%".$search."%";
        $stmt->bind_param("s",$search);
        $stmt->execute();
        $result=$stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    /**restituisce tutti gli organizzatore */
    public function getOrganizer(){
        $query="SELECT Codice, Nome, Cognome,Email, DataNascita, Indirizzo FROM Utente WHERE Tipo='organizzatore' ORDER BY Cognome ASC";
        $stmt=$this->db->prepare($query);
        $stmt->execute();
        $result=$stmt->get_Result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    /**restituisce eventi per Admin */
    public function getAllEvents(){
        $query = "SELECT evento.Codice, evento.nome, DescrLunga, DescrBreve, categoria.Nome AS nomeCategoria, utente.Nome AS nomeOrganizzatore, NomeImmagine 
        FROM Evento
        INNER JOIN categoria ON evento.CodiceCategoria=categoria.Codice 
        INNER JOIN utente ON evento.CodiceOrganizzatore=utente.Codice
        ORDER BY evento.nome ASC" ;
        $stmt=$this->db->prepare($query);
        $stmt->execute();
        $result=$stmt->get_Result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    public function getAdmin(){
        $query="SELECT Codice, Nome, Cognome,Email, DataNascita, Indirizzo FROM Utente WHERE Tipo='admin' ORDER BY Cognome ASC";
        $stmt=$this->db->prepare($query);
        $stmt->execute();
        $result=$stmt->get_Result();
        return $result->fetch_all(MYSQLI_ASSOC);

    }
    /**elimina organizzatore admin */
    public function removeOrganizer($CodiceOrganizzatore){
        $query="DELETE FROM utente WHERE Codice=?";
        $stmt=$this->db->prepare($query);
        $stmt->bind_param("i",$CodiceOrganizzatore);
        $stmt->execute();
    }
    /**elimina evento admin*/
    public function removeEvent($CodiceEvento){
        $query="DELETE FROM evento WHERE Codice=?";
        $stmt=$this->db->prepare($query);
        $stmt->bind_param("i",$CodiceEvento);
        return $stmt->execute();
    }   
    /**ritorna gli ordini*/
    public function myOrder($codiceUtente){
        $query="SELECT ordine.Codice AS codiceOridne,Quantita,ordine.Data AS dataOrdine,dettaglioevento.Data AS dataEvento, Luogo,Ora,evento.Codice AS codiceEvento, evento.nome, DescrLunga, DescrBreve, NomeImmagine
        FROM ordine
        INNER JOIN dettaglioevento ON ordine.CodDettaglioEvento=dettaglioevento.Codice
        INNER JOIN evento ON evento.Codice=dettaglioevento.CodiceEvento
        WHERE ordine.CodUtente=? ORDER BY ordine.Data DESC";
        $stmt=$this->db->prepare($query);
        $stmt->bind_param("i",$codiceUtente);
        $stmt->execute();
        $result=$stmt->get_Result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }


    public function getAvailableTickets($codDettaglio){
        $query = "SELECT BigliettiDisponibili FROM dettaglioevento WHERE Codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$codDettaglio);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_assoc();
    }

    public function insertOrder($codDettaglio, $quantita, $codUtente){
        $stmt = $this->db->prepare("INSERT INTO ordine (CodDettaglioEvento, Quantita, CodUtente) VALUES (?, ?, ?)");
        $stmt->bind_param('iii',$codDettaglio, $quantita, $codUtente);
        return $stmt->execute();
    }

    public function updateAvailableTickets($codDettaglio, $b){
        $query = "UPDATE DettaglioEvento SET BigliettiDisponibili = ? WHERE Codice = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('is', $b, $codDettaglio);
        return $stmt->execute();
    }
    public function getEventForOrganizer($organizzatore){
         $query="SELECT Codice, codiceOrganizzatore FROM evento WHERE evento.CodiceOrganizzatore=?";
         $stmt = $this->db->prepare($query);
         $stmt->bind_param('i',$organizzatore);
         $stmt->execute();
         $result=$stmt->get_Result();
         return $result->fetch_all(MYSQLI_ASSOC);
     }
}
?>