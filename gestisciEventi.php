<?php
require_once 'connection.php';
//notifiche
if(isset($_SESSION["idUtente"])){
   $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }
 //base Template
 $templateParams["titolo"] = "JumpTheLine - Gestisci Eventi";
 $templateParams["pagina"] = "gestisciEventi.php";

 $templateParams["eventi"]=$dbh->getAllEvents();
 if (isset($_GET["evento"])){
    $dbh->removeEvent($_GET["evento"]);
    $templateParams["eventi"]=$dbh->getAllEvents();   
   }
 require 'template/base.php';
?>