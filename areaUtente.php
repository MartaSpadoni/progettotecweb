<?php
require_once 'connection.php';
//Base Template
$templateParams["titolo"] = "JumpTheLine - Area Utente";
$templateParams["pagina"] = "areaUtente.php";
if(isset($_SESSION["idUtente"])){
   $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }
 $templateParams["utente"] = $dbh->getUserById($_SESSION["idUtente"]);
if($_SESSION["tipo"] == 'cliente'){
$templateParams["sezioni"]= array(
    array("I miei dati", "userData.png", "datiUtente.php"), 
    array("I miei biglietti", "userTicket.png", "mieiBiglietti.php"), 
    array("Notifiche", "notifiche.png", "notifiche.php"),
    array("Help", "help.png", "help.php"));
}elseif($_SESSION["tipo"] == 'organizzatore'){
    $templateParams["sezioni"]= array(
       array("Crea Evento", "creaEvento.png", "creazioneEvento.php"), 
       array("I miei eventi", "gestisciEvento.png", "mieiEventi.php"), 
       array("Notifiche", "notifiche.png", "notifiche.php"),
       array("I miei dati", "userData.png", "datiUtente.php"));
}else{
   $templateParams["sezioni"]= array(
      array("Gestisci Eventi", "gestisciEvento.png", "gestisciEventi.php"), 
      array("Gestisci Organizzatori", "userData.png", "gestisciOrganizzatore.php"));
}

require 'template/base.php';
?>