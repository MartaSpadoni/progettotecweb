<?php
require_once 'connection.php';
$templateParams["evento"] = $dbh->getEventById($_GET["id"]);
//Base Template
$templateParams["titolo"] = "JumpTheLine -".$templateParams["evento"]["nome"];
$templateParams["pagina"] = "dettaglioArtista.php";
if(isset($_SESSION["idUtente"])){
   $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }
$templateParams["DettaglioEvento"] = $dbh->getEventDetails($_GET["id"]);

require 'template/base.php';
?>