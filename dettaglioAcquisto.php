<?php
require_once 'connection.php';
//Base Template
$templateParams["titolo"] = "JumpTheLine - Carrello";

if(isset($_SESSION["idUtente"])){
   $templateParams["pagina"] = "dettaglioAcquisto.php";
   $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
    $templateParams["pagina"] = "login.php";
 }
// qui ci va la chiamata al db che ritorna il dettaglio evento dato il codice passato in get
$templateParams["dettagliEvento"] = $dbh->getEventDetailsById($_GET["dettaglioEvento"]);
$templateParams["evento"] = $dbh->getEventById($templateParams["dettagliEvento"]["CodiceEvento"]);


require 'template/base.php';
?>