<?php
require_once 'connection.php';  

$templateParams["titolo"] = "JumpTheLine - Login";
$templateParams["pagina"] = "login.php";
if(isset($_SESSION["idUtente"])){
   $numeroNotifiche = count($dbh-> getUnreadNotifications($_SESSION["idUtente"]));
    $templateParams["user"] = $dbh->getUserById($_SESSION["idUtente"]);
    if($numeroNotifiche > 0){
       $templateParams["numeroNotifiche"] = $numeroNotifiche;
    }else{
       $templateParams["numeroNotifiche"]="";
    }
 }else{
    $templateParams["numeroNotifiche"] = "";
 }

    if(isset($_POST["email"]) && isset($_POST["p"])){
        $email = $_POST['email'];
        $password = $_POST['p']; // Recupero la password criptata.
        $user = $dbh->getUserByEmail($email);
        //dall'array restituito prendo salt e db_password
        if(!empty($user) && login($email, $password, $user['Salt'], $user['Password'], $user['Codice'], $user['Nome'], $user['Tipo']) ) {
            header("Location: ./");
        } else {
                // Login fallito
                $templateParams["failure"] = 'Email o password errati!';
                // header('Location: ./login.php?error=1');
        } 
    } 

require 'template/base.php';
?>